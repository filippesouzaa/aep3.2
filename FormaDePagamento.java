
public class FormaDePagamento {

private double totalDeDebitos;
private CompraEfetuadaListener listener;

public void comprar(double valor) {
    if (listener != null) {
        listener.notificar(valor);
    }
    this.totalDeDebitos += valor;
}

public void ativarNotificacao(CompraEfetuadaListener listener) {
    this.listener = listener;
}

public void desativarNotificacao() {
    this.listener = null;
    }
}