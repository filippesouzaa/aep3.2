public abstract class FabricaDeBolo {

	public static Outro criarConfeito(Sabor sabor) {
		if (sabor == Sabor.ADOCICADO) {
			return new Bolaria();
		} else if (sabor == Sabor.ACIDO){
			return new BoloDeLimao();
			//return new TorteleteDeFrutasCitricas();
		}
		return null;
	}

}
